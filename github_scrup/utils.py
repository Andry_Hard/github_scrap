import json, sys, logging
from typing import Any, Tuple, Union

logging.basicConfig(
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    level=logging.INFO,
    stream=sys.stdout
)
logger = logging.getLogger()


root_by_type = {
    'repositories': {
        'object': '//*[@id="js-pjax-container"]/div/div[3]/div/ul/li/div[2]/div[1]/a/@href',
    },
    'issues': {
        'object': '//*[@id="issue_search_results"]/div[1]/div/div/div/div[2]/a/@href',
    },
    'wikis': {
        'object':'//*[@id="wiki_search_results"]/div[1]/div/div[1]/a/@href',
    }
}

def get_path_by_type(result_type:str) -> Union[str, None]:
    try:
        root = root_by_type.get(result_type.lower())
        return root['object']
    except:
        logger.warning("Unexpected type")
        return None


def get_input_data(input_json) -> Union[Tuple, None]:
    try:
        data = input_json
        keywords = u",".join(data.get('keywords'))
        proxies = {"proxies": data.get('proxies')}
        result_type = data.get('type', 'repositories')
        input_data = (keywords,proxies,result_type)
        return input_data
    except:
        logger.warning("Incorrect format input_data")
        return None

def get_input_file(input_file):
    with open(input_file) as f:
        data = json.load(f)
    return data
