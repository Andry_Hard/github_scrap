import unittest
import requests
import github_scrap
import github_scrap_second
from utils import get_input_data, get_path_by_type


class MainTest(unittest.TestCase):

    def test_input(self):
        input1 = {
            "keywords": [
                "openstack",
                "nova",
                "css"
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ],
            "type": "wikis"
        }
        input2 = {
            "key": [
                "openstack",
                "nova",
                "css"
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ],
            "type": "wikis"
        }
        input3 = {
            "keywords": [
                "openstack",
                "nova",
                "css"
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ]
        }
        val1 = get_input_data(input1)
        val2 = get_input_data(input2)
        val3 = get_input_data(input3)
        self.assertTrue(val1)
        self.assertEqual(val2, None)
        self.assertTrue(val3)

    def test_connection(self):
        base_url = f'https://github.com/search?q=python'
        r = requests.get(base_url)
        self.assertEqual(r.status_code, 200)

    def test_github_scrap(self):
        input = {
            "keywords": [
                "django",
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ],
            "type": "repositories"
        }
        output_exepted1 = {'url': 'https://github.com/django/django'}
        output_exepted2 = {'url': 'https://github.com/encode/django-rest-framework'}
        output = github_scrap.run(input)
        self.assertIn(output_exepted1, output)
        self.assertIn(output_exepted2, output)

    def test_github_scrap_unicode_enabled(self):
        input = {
            "keywords": [
                "пітон",
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ],
            "type": "Repositories"
        }
        output_exepted = {'url': 'https://github.com/Hard7Rock/python_diag'}
        output = github_scrap.run(input)
        self.assertIn(output_exepted, output)

    def test_github_scrap_second(self):
        input = {
            "keywords": [
                "openstack",
            ],
            "proxies": [
                "194.126.37.94:8080",
                "13.78.125.167:8080"
            ],
            "type": "repositories"
        }
        output_exepted = {'url': 'https://github.com/openstack/openstack', 'extra': {'owner': 'openstack', 'language_stats': {'Python': '100.0%'}}}
        output = github_scrap_second.run(input)
        self.assertIn(output_exepted, output)


if __name__ == "__main__":
   unittest.main()
