import requests, sys, logging
from lxml import html
from utils import get_input_data, get_path_by_type, get_input_file


input_json = {
    "keywords": [
        "openstack",
        "django",
    ],
    "proxies": [
        "194.126.37.94:8080",
        "13.78.125.167:8080"
    ],
    "type": "Repositories"
}

logging.basicConfig(
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    level=logging.INFO,
    stream=sys.stdout
)
logger = logging.getLogger()


def run(input_json):

    keywords,proxies,result_type = get_input_data(input_json)
    path = get_path_by_type(result_type)
    print(path)
    root_lang = '//*[@id="js-repo-pjax-container"]/div[2]/div/details/div/ol/li/a'
    result = []

    with requests.Session() as session:
        response = session.get(f'https://github.com/search?q={keywords}&type={result_type}', proxies=proxies)
        three = html.fromstring(response.content)
        for obj_base in three.xpath(f'{path}'):
            repo = session.get(f"https://github.com{obj_base}", proxies=proxies)
            repo_three = html.fromstring(repo.content)
            owner = obj_base.split('/')[1]
            stats = {}
            for lang in repo_three.xpath(f'{root_lang}'):
                l_name = lang.xpath('./span[@class="lang"]/text()')[0]
                l_stats = lang.xpath('./span[@class="percent"]/text()')[0]
                stats[f"{l_name}"] = l_stats
            obj = {
                "url": f"https://github.com{obj_base}",
                "extra": {
                    "owner": f"{owner}",
                    "language_stats": stats
                }
            }
            result.append(obj)
    logging.info(result)
    return result


if __name__=='__main__':
    print(f"Arguments count: {len(sys.argv)}")
    try:
        input_file = sys.argv[1]
        input_data = get_input_file(input_file)
        run(input_data)
    except:
        run(input_json)
