import requests, sys, logging
from lxml import html
from utils import get_path_by_type, get_input_data, get_input_file


input_json = {
    "keywords": [
        "пітон",
    ],
    "proxies": [
        "194.126.37.94:8080",
        "13.78.125.167:8080"
    ],
    "type": "repositories"
}

logging.basicConfig(
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    level=logging.INFO,
    stream=sys.stdout
)
logger = logging.getLogger()


def run(input_json):

    keywords,proxies,result_type = get_input_data(input_json)
    path = get_path_by_type(result_type)

    result = []

    with requests.Session() as session:
        base_url = f'https://github.com/search?q={keywords}&type={result_type}'

        response = session.get(base_url, proxies=proxies)
        three = html.fromstring(response.content)
        for obj_link in three.xpath(f"{path}"):
            obj = {
                "url": f"https://github.com{obj_link}"
            }
            result.append(obj)
    logger.info(result)
    return result


if __name__=='__main__':
    logger.info(f"Arguments count: {len(sys.argv)}")
    try:
        input_file = sys.argv[1]
        logger.info(f"Input file - {input_file}")
        input_data = get_input_file(input_file)
        run(input_data)
    except:
        run(input_json)
