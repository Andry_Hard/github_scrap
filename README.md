# github_scrap
==============

Simple scrapers. 

This scripts print result into terminal. These can be modify.

1 Installation
---------------

>git clone https://gitlab.com/Andry_Hard/github_scrap.git 

Create environmet and install requirements 

>pip install -r req.txt 

2 Usage
----------

Move on to directory github_scrub 

You should modify execute permission 

>chmod 550 github_scrab.py 

>chmod 550 github_scrab_second.py

And run this scripts

>python github_scrub.py 

>python github_scrub_second.py

Also you can run with custom file


>python <script_name> <file_path>


For example 

>python github_scrap.py input_file,json

